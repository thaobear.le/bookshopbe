import { BaseEntity, Column, Entity, Index } from "typeorm";

@Index("admin_pkey", ["id"], { unique: true })
@Entity("admin", { schema: "public" })
export class Admin extends BaseEntity {
  @Column("uuid", {
    primary: true,
    name: "id",
    default: () => "uuid_generate_v4()",
  })
  id: string;

  @Column("character varying", { name: "email", length: 255 })
  email: string;

  @Column("character varying", { name: "password", length: 255 })
  password: string;

  @Column("character varying", { name: "full_name", length: 255 })
  fullName: string;
}
