import { Injectable, HttpException, UnauthorizedException, BadRequestException } from '@nestjs/common';
import { AddAdminDto } from './dto/admin.dto';
import { Password } from '../utils/password';
import { AdminRepository } from './admin.repository';
import { SignInDto } from './dto/signin.dto';
import { JwtService } from '@nestjs/jwt';
import { ChangePasswordDto } from './dto/change-password.dto';
import { Admin } from './admin.entity';

@Injectable()
export class AdminService {
  constructor(
    private adminRepository: AdminRepository,
    private jwtService:JwtService
  ){}
  async createAdmin(addAdminDto:AddAdminDto) {
    const {password,email,fullName} = addAdminDto
    const user = await this.adminRepository.findOne({email});
    if(user) {
      throw new HttpException('Admin exist',400)
    }
    const passwordStore = await Password.toHash(password)
    const newUser = await this.adminRepository.insert({password:passwordStore,email,fullName})
    return newUser
  }

  async signIn(signInDto:SignInDto) {
    const {email,password} = signInDto
    const user = await this.adminRepository.findOne({email})
    console.log(user)
    if(!user) {
      throw new HttpException('Admin Not Found',404);
    }
    const checkPassword = await Password.compare(user.password,password)
    if(!checkPassword) {
      throw new UnauthorizedException();
    }
    const payload = {
      email:user.email,
    }
    const token = this.jwtService.sign(payload);
    return {token}
  }

  async changePassword(admin:Admin,changePasswordDto:ChangePasswordDto) {
    const {oldPassword,newPassowrd} = changePasswordDto
    const {id} = admin
    const oldUser = await this.adminRepository.findOne({
      where:{
        id,
      },
    })
    const check = await Password.compare(oldUser.password,oldPassword)
    if(!check)  {
      throw new BadRequestException("Password is not corret")
    }
    const newPass = await Password.toHash(newPassowrd)
    oldUser.password = newPass;
    const newAdmin = await oldUser.save();
    return newAdmin
  }
}
