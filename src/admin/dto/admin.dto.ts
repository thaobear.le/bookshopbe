import { IsNotEmpty, IsString, IsBoolean, IsEmail, MinLength } from 'class-validator';
export class AddAdminDto {

    @IsNotEmpty()
    @IsString()
    email: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(8)
    password: string;

    @IsNotEmpty()
    @IsString()
    fullName: string;


}