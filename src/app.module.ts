import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm'
import {ConfigModule,ConfigService} from '@nestjs/config'
// import { AdminModule } from './admin/admin.module';
import { CartModule } from './cart/cart.module';
import { CartBookModule } from './cart-book/cart-book.module';
import { OrderBookModule } from './order-book/order-book.module';
import { OrderModule } from './order/order.module';
import { BookImageModule } from './book-image/book-image.module';
import { ReviewModule } from './review/review.module';
import { CategoryModule } from './category/category.module';
import { PromotionModule } from './promotion/promotion.module';
import { Category } from './category/entities/category.entity';
import {SnakeNamingStrategy} from 'typeorm-naming-strategies';
import {  CustomerModule } from "./customer/customer.module";
import { BookModule } from './book/book.module';
import { AdminModule } from './admin-sang/admin.module';
import { MulterModule } from '@nestjs/platform-express';

import { ServeStaticModule } from '@nestjs/serve-static/dist/serve-static.module';
import { join } from 'path';



@Module({
  imports: [

        ServeStaticModule.forRoot({
            rootPath: join(__dirname, '..', 'files'),
        }),
   MulterModule.register({
      dest: './files',
    }),
    ConfigModule.forRoot({
      envFilePath:`env/${process.env.NODE_ENV || 'local'}.env`,
    }),
    TypeOrmModule.forRootAsync({
      imports:[ConfigModule],
      inject:[ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        synchronize: false,
        host: configService.get<string>('DATABASE_HOST'),
        port: Number(configService.get<string>('DATABASE_PORT')),
        username: configService.get<string>('DATABASE_USER_NAME'),
        password: configService.get<string>('DATABASE_PASSWORD'),
        database: configService.get<string>('DATABASE_NAME'),
        url:configService.get<string>('DATABASE_URL'),
        entities: [__dirname + '/**/*.entity.{js,ts}'],
        ssl: true,
        extra: {
          ssl: {
            rejectUnauthorized: false,
          },
        },
      
        logging:true,
        namingStrategy: new SnakeNamingStrategy()
      })
    }),
    BookModule,
    CustomerModule,
    AdminModule,
    CategoryModule,
    PromotionModule,
    ReviewModule,
    BookImageModule,
    OrderModule,
    OrderBookModule,
    CartBookModule,
    CartModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
