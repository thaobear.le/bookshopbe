import { Test, TestingModule } from '@nestjs/testing';
import { BookImageController } from './book-image.controller';
import { BookImageService } from './book-image.service';

describe('BookImageController', () => {
  let controller: BookImageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BookImageController],
      providers: [BookImageService],
    }).compile();

    controller = module.get<BookImageController>(BookImageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
