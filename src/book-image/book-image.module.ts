import { Module } from '@nestjs/common';
import { BookImageService } from './book-image.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookImage } from './entities/book-image.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([BookImage])
  ],
  providers: [BookImageService]
})
export class BookImageModule {}
