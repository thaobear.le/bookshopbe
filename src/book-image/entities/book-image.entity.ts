import { Book } from "src/book/entities/book.entity";
import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
  } from "typeorm";
  
  
  @Index("bookimage_pkey", ["bookId", "imageSrc"], { unique: true })
  @Entity("book_image", { schema: "public" })
  export class BookImage extends BaseEntity {
    @Column("uuid", { primary: true, name: "book_id" })
    bookId: string;
  
    @Column("character varying", {
      primary: true,
      name: "image_src",
      length: 255,
    })
    imageSrc: string;
  
    @ManyToOne(() => Book, (book) => book.bookImages)
    @JoinColumn([{ name: "book_id", referencedColumnName: "id" }])
    book: Book;
  }
  