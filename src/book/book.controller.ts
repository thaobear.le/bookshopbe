import { Controller, Get, Post, Body, Put, Param, Delete, Req, Res, UseInterceptors, UploadedFile, UseGuards ,} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from "@nestjsx/crud";
import { BookService } from './book.service';
import { Book } from './entities/book.entity';
import { AdminGuard } from '../guards/admin.guard';
import { editFileName, imageFileFilter } from 'src/utils/file-uploading.utils';
import { diskStorage } from 'multer';


@ApiTags('book')
@Controller('book')
export class BookController {
  constructor(public service: BookService) {
  }
  @Post()
  @UseGuards(AdminGuard)
  @UseInterceptors(
    FileInterceptor('image', {
      storage: diskStorage({
        destination: './files',
        filename: editFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  async create(@UploadedFile() file, @Body() body:Book) {
    try {
      console.log(body)
      return this.service.uploadImage(file, body);
    } catch (error) {
      return {error:error.message}
    }
  }


//   @Post('/image')
  
//     async uploadedFile(@UploadedFile() file) {
//       const response = {
//         originalname: file.originalname,
//         filename: file.filename,
//       };
//       return this.service.uploadFile(file);
//     }

//     @Get('/image/:imgpath')
// seeUploadedFile(@Param('imgpath') image, @Res() res) {
//   return res.sendFile(image, { root: './files' });
// }

  @UseGuards(AdminGuard)
  @Put('delete/:id')
  deleteOne(
    @Param('id') id: string
  ){
    return this.service.delete(id);
  }

  @Put('/:id')
  updateBook(
    @Body() dto:Book,
    @Param('id') id:string
  ){
    console.log(dto)
    return this.service.update(dto,id)
  }

  @Get('/')
  getAll() {
    return this.service.getAll()
  }

  @Get('/:id')
  getOne(
    @Param('id') id: string
  ) {
    return this.service.getOne(id)
  }

 
 
}
