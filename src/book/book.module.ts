import { Module } from '@nestjs/common';
import { BookService } from './book.service';
import { BookController } from './book.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookRepository } from './book.repository';
import { Book } from './entities/book.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([Book])
  ],
  exports:[BookService],
  controllers: [BookController],
  providers: [BookService]
})
export class BookModule {}
