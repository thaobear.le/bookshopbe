import { Req, Res, Injectable, NotFoundException } from '@nestjs/common';
import * as multer from 'multer';
import { S3 } from 'aws-sdk';
import * as multerS3 from 'multer-s3';
import { InjectRepository } from '@nestjs/typeorm';
import { Book } from './entities/book.entity';
import { Repository, getRepository, DeleteResult } from 'typeorm';
import { BookImage } from '../book-image/entities/book-image.entity';

const AWS_S3_BUCKET_NAME = 'book-tmdt';
interface S3Result {
    ETagg:string
    Location:string

}
@Injectable()
export class BookService {
  constructor(
    @InjectRepository(Book)
    private repo :Repository<Book>
  ) {}

  async uploadImage(file, body:Book) {
    
    console.log(body);
   
    const bucketS3 = process.env.AWS_S3_BUCKET_NAME;
    try {
      if (file) {
        const response = {
          originalname: file.originalname,
          filename: file.filename,
        };
      const newBook = await this.repo.create(body);
      await newBook.save();
      await getRepository(BookImage).insert({bookId:newBook.id,imageSrc:response.filename});
      return {...newBook,imageSrc:response.filename}
      }
      else {
         const newBook = await this.repo.create(body);
        await newBook.save();
        return {...newBook} }
      


    } catch (error) {
      return error.message;
    }
  }

// async uploadFile(file){
//   const response = {
//     originalname: file.originalname,
//     filename: file.filename,
//   };
//   return response;
// }

  async uploadS3(file, bucket, name) {
    const s3 = this.getS3();
    const params = {
      Bucket: bucket,
      Key: String(name),
      ACL: 'public-read-write',
      Body: file,
    };
    return new Promise<S3Result>((resolve, reject) => {
      s3.upload(params, (err, data) => {
        if (err) {
          console.log(err);
          reject(err.message);
        }
        resolve(data);
      });
    });
  }

  getS3() {
    return new S3({
      accessKeyId: process.env.KEY_ID,
      secretAccessKey: process.env.ACCESS_KEY,
    });
  }
  async getAll() {
    return await this.repo.find({
      relations:['reviews','reviews.customer','bookImages'],
      order:{
        createdAt:'DESC'
      },
    })
  }

  async update(dto,id) {
    console.log(dto)
    const book = await this.repo.findOne({id});
    if(!book) {
      throw new NotFoundException();
    }
    await this.repo.update({id},dto);
    return await this.repo.findOne({id})
  }

  async getOne(id) {
    return await this.repo.find({
      where:{
        id
      },
      relations:['reviews','reviews.customer','bookImages'],
      order:{
        createdAt:'DESC'
      },
    })
  }
  async delete(id) {
    const customer = await this.repo.findOne({id});
  if(!customer) {
    throw new NotFoundException();
  }
  await this.repo.update({id},{deleted: true});
  return {message:"Deleted Sucessfully"}
}
}
