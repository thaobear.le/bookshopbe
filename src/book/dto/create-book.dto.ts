export class CreateBookDto {
  title: string;
  author: string;
  rate: number;
  countRate: number;
  publicationDate: Date;
  pricePerUnit: number;
  quantity: number;
  discount: number;
  createdAt: Date;
  description: string;
publisher: string
isbn: string
categoryName: string;
categoryId:string
size: string
deleted: boolean ;

noPage: number;
}
