import { BookImage } from "src/book-image/entities/book-image.entity";
import { CartBook } from "src/cart-book/entities/cart-book.entity";
import { Category } from "src/category/entities/category.entity";
import { OrderBook } from "src/order-book/entities/order-book.entity";
import { Review } from "src/review/entities/review.entity";
import { BaseEntity, Column, Entity, Index, OneToMany , ManyToOne , JoinColumn} from "typeorm";

@Index("book_pkey", ["id"], { unique: true })
@Entity("book", { schema: "public" })
export class Book extends BaseEntity {
  @Column("uuid", {
    primary: true,
    name: "id",
    default: () => "uuid_generate_v4()",
  })
  id: string;

  @Column("uuid", {
    name: "category_id"
  })
  categoryId: string;
@Column("varchar",{name:"publisher",length:255})
publisher: string
@Column("varchar",{name:"isbn",length:255})
isbn: string

@Column("varchar",{name:"size",length:50})
size: string
@Column("boolean",{name:"deleted", nullable:true, default: () => "false" })
deleted: boolean | null;

@Column("smallint",{name:"no_page"})
noPage: number;

  @Column("character varying", { name: "category_name", length: 50 })
  categoryName: string;

  @Column("character varying", { name: "title", length: 255 })
  title: string;

  @Column("character varying", { name: "author", nullable: true, length: 255 })
  author: string | null;

  @Column("smallint", { name: "rate", nullable: true })
  rate: number | null;

  @Column("smallint", { name: "count_rate", nullable: true })
  countRate: number | null;

  @Column("timestamp without time zone", {
    name: "publication_date",
    nullable: true,
  })
  publicationDate: Date | null;

  @Column("integer", { name: "price_per_unit", nullable: true })
  pricePerUnit: number | null;

  @Column("smallint", { name: "quantity", default: () => "0" })
  quantity: number;

  @Column("smallint", { name: "discount", nullable: true })
  discount: number | null;

  @Column("timestamp without time zone", {
    name: "created_at",
    nullable: true,
    default: () => "CURRENT_TIMESTAMP",
  })
  createdAt: Date | null;

  @Column("text", { name: "description", nullable: true })
  description: string | null;

  @OneToMany(() => BookImage, (bookImage) => bookImage.book)
  bookImages: BookImage[];

  @OneToMany(() => CartBook, (cartBook) => cartBook.book)
  cartBooks: CartBook[];

  @OneToMany(() => OrderBook, (orderBook) => orderBook.book)
  orderBooks: OrderBook[];

  @OneToMany(() => Review, (review) => review.book)
  reviews: Review[];

 @ManyToOne(() => Category, (category) => category.books)
    @JoinColumn([{ name: "category_id", referencedColumnName: "id" }])
    category: Category;
}
