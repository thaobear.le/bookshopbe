import { Test, TestingModule } from '@nestjs/testing';
import { CartBookController } from './cart-book.controller';
import { CartBookService } from './cart-book.service';

describe('CartBookController', () => {
  let controller: CartBookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CartBookController],
      providers: [CartBookService],
    }).compile();

    controller = module.get<CartBookController>(CartBookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
