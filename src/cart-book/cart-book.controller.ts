import { Controller, Get, Post, Body, Put, Param, Delete, ValidationPipe, UseGuards } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import { ApiTags } from '@nestjs/swagger';

import { CartBookService } from './cart-book.service';
import { CreateCartBookDto } from './dto/create-cart-book.dto';
import { getUser } from 'src/customer/get-user.decorator';
import { Customer } from '../customer/entities/customer.entity';
import { CustomerGuard } from '../guards/customer.guard';

@ApiTags('CartBook')
@Controller('cart-book')
@UseGuards(CustomerGuard)
export class CartBookController {
  constructor(public service: CartBookService) {}

  @Post('/update-cart')
  addBook(
    @Body(ValidationPipe) dto:CreateCartBookDto,
    @getUser() user: Customer,
  ){
    return this.service.addBook(dto,user)
  }
  




}
