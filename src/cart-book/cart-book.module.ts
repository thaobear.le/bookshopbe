import { Module } from '@nestjs/common';
import { CartBookService } from './cart-book.service';
import { CartBookController } from './cart-book.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CartBook } from './entities/cart-book.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([CartBook])
  ],
  controllers: [CartBookController],
  providers: [CartBookService]
})
export class CartBookModule {}
