import { Test, TestingModule } from '@nestjs/testing';
import { CartBookService } from './cart-book.service';

describe('CartBookService', () => {
  let service: CartBookService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CartBookService],
    }).compile();

    service = module.get<CartBookService>(CartBookService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
