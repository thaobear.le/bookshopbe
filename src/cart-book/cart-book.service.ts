import { Injectable, Controller } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import {TypeOrmCrudService} from '@nestjsx/crud-typeorm'
import { InjectRepository } from '@nestjs/typeorm';
import { CartBook } from './entities/cart-book.entity';
import { DeleteResult, getRepository } from 'typeorm';
import { Cart } from '../cart/entities/cart.entity';
import { CreateCartBookDto } from './dto/create-cart-book.dto';
import { Customer } from '../customer/entities/customer.entity';
import { Book } from '../book/entities/book.entity';
;
@Injectable()
export class CartBookService extends TypeOrmCrudService<CartBook> {
  constructor(@InjectRepository(CartBook) repo) {
    super(repo);
  }
  
  async addBook(dto:CreateCartBookDto,user:Customer) {
    const cart  = await getRepository(Cart).findOne({customerId:user.id})
    const {bookId,quantity} = dto
    if(!cart) {
      const book = await getRepository(Book).findOne(bookId)
      const newCart = await getRepository(Cart).create({customerId:user.id,totalQuantity:quantity,totalBookPrice:Number(book.pricePerUnit) * quantity})
      await newCart.save();
      await this.repo.insert({cartId:newCart.id,bookId,quantity})
    }
    else {
      const book = await this.repo.findOne({cartId:cart.id,bookId});
      if(book) {
        if(quantity ===0){
          await this.repo.delete({cartId:cart.id,bookId})
        }
        else {
          await this.repo.update({cartId:cart.id,bookId},{quantity})
        }
      }
      else {
        console.log('yes')
        await this.repo.insert({cartId:cart.id,bookId,quantity})
      }
      const query = await this.repo.createQueryBuilder("cb")
        .select(['SUM(cb.quantity * b.pricePerUnit *(100-b.discount) /100) as total_price,SUM(cb.quantity) as total_quantity'])
        .where("cb.cartId = :cartId",{cartId:cart.id})
        .innerJoin("cb.book","b")
        .groupBy("cb.cartId")
        .getRawOne();
      await getRepository(Cart).update({id:cart.id},{totalBookPrice:query.total_price,totalQuantity:query.total_quantity})
    }
    return await getRepository(Cart).findOne({where:{id:cart.id}, relations:['cartBooks','cartBooks.book']})
  }
  
}