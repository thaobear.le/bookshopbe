import { IsNotEmpty } from "class-validator"

export class CreateCartBookDto {
  @IsNotEmpty()
  bookId:	string
  @IsNotEmpty()
  quantity:	number
}
