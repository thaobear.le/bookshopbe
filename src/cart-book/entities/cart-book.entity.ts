import { Book } from "src/book/entities/book.entity";
import { Cart } from "src/cart/entities/cart.entity";
import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
  } from "typeorm";
  
  @Index("cart_book_pkey", ["bookId", "cartId"], { unique: true })
  @Entity("cart_book", { schema: "public" })
  export class CartBook extends BaseEntity {
    @Column("uuid", { primary: true, name: "cart_id" })
    cartId: string;
  
    @Column("uuid", { primary: true, name: "book_id" })
    bookId: string;
  
    @Column("smallint", { name: "quantity", nullable: true })
    quantity: number | null;
  
    @Column("timestamp without time zone", {
      name: "created_at",
      nullable: true,
      default: () => "CURRENT_TIMESTAMP",
    })
    createdAt: Date | null;
  
    @ManyToOne(() => Book, (book) => book.cartBooks)
    @JoinColumn([{ name: "book_id", referencedColumnName: "id" }])
    book: Book;
  
    @ManyToOne(() => Cart, (cart) => cart.cartBooks)
    @JoinColumn([{ name: "cart_id", referencedColumnName: "id" }])
    cart: Cart;

    
  }
  