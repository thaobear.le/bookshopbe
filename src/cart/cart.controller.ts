
import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from "@nestjsx/crud";
import { Cart } from './entities/cart.entity';
import { CartService } from './cart.service';
import { CustomerGuard } from '../guards/customer.guard';
import { getUser } from 'src/customer/get-user.decorator';
import { Customer } from 'src/customer/entities/customer.entity';

@ApiTags('Cart')
// @Crud({
//   model:{
//     type:Cart,
//   },
//   params: {
//     id: {
//       field:'id',
//       type:'uuid',
//       primary:true,
//     }
//   },
//   routes:{
//     only:['getOneBase']
//   },
//   query:{
//     join: {
//       cartBooks: {
//         eager:true,
//       }
//     }
//   }
// })
@UseGuards(CustomerGuard)
@Controller('cart')
export class CartController implements CrudController<Cart> {
  constructor(public service: CartService) {}
  @Get('/')
  getCart(
    @getUser() user:Customer,
  ) {
    return this.service.getCart(user)
  }

}
