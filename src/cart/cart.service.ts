import { Injectable, Controller } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import {TypeOrmCrudService} from '@nestjsx/crud-typeorm'
import { InjectRepository } from '@nestjs/typeorm';
import { Cart } from './entities/cart.entity';
import { Customer } from 'src/customer/entities/customer.entity';



@Injectable()
export class CartService extends TypeOrmCrudService<Cart> {
  constructor(@InjectRepository(Cart) repo) {
    super(repo);
  } 
  async getCart(user:Customer) {
    const cart = await this.repo.findOne({
      where:{
        customerId:user.id
      }
    })
    const cartId = cart.id;
    const query = await this.repo.createQueryBuilder("c")
        .select(['SUM(cb.quantity * b.pricePerUnit *(100-b.discount) / 100 ) as total_price,SUM(cb.quantity) as total_quantity'])
        .where("c.id = :id",{id:cartId})
        .innerJoin("c.cartBooks","cb")
        .innerJoin("cb.book","b")
        .groupBy("cb.cartId")
        .getRawOne();
      await this.repo.update({id:cartId},{totalBookPrice:query.total_price,totalQuantity:query.total_quantity})
  
      return await this.repo.findOne({
        where:{
          id:cartId
        },
        relations:['cartBooks','cartBooks.book']
      })
  }
}