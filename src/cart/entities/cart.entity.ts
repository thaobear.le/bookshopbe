import { CartBook } from "src/cart-book/entities/cart-book.entity";
import { Customer } from "src/customer/entities/customer.entity";
import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    OneToOne,
  } from "typeorm";
  
  @Index("cart_pkey", ["id"], { unique: true })
  @Entity("cart", { schema: "public" })
  export class Cart extends BaseEntity {
    @Column("uuid", {
      primary: true,
      name: "id",
      default: () => "uuid_generate_v4()",
    })
    id: string;
  
    @Column()
    customerId:string;
    
    @Column("smallint", { name: "total_quantity", nullable: true })
    totalQuantity: number | null;
  
    @Column("integer", { name: "total_book_price", nullable: true })
    totalBookPrice: number | null;
  
    @Column("timestamp without time zone", {
      name: "created_at",
      nullable: true,
      default: () => "CURRENT_TIMESTAMP",
    })
    createdAt: Date | null;
  
    @Column("smallint", { name: "tax", nullable: true })
    tax: number | null;
  
    @OneToOne(() => Customer, (customer) => customer.cart)
    @JoinColumn([{ name: "customer_id", referencedColumnName: "id" }])
    customer: Customer;
  
    @OneToMany(() => CartBook, (cartBook) => cartBook.cart)
    cartBooks: CartBook[];
  }
  