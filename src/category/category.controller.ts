import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from "@nestjsx/crud";
import { Category } from './entities/category.entity';
import { CategoryService } from './category.service';
import { AdminGuard } from '../guards/admin.guard';

@ApiTags('Category')
@Crud({
  model:{
    type:Category,
  },
  params: {
    id: {
      field:'id',
      type:'uuid',
      primary:true
    }
  },
})
@UseGuards(AdminGuard)
@Controller('category')
export class CategoryController implements CrudController<Category> {
  constructor(public service: CategoryService) {}

}
