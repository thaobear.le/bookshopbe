import { Injectable, Controller } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import {TypeOrmCrudService} from '@nestjsx/crud-typeorm'
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from './entities/category.entity';
@Injectable()
export class CategoryService extends TypeOrmCrudService<Category> {
  constructor(@InjectRepository(Category) repo) {
    super(repo);
  } 
}