import { Book } from "src/book/entities/book.entity";
import { BaseEntity, Column, Entity, Index, OneToMany } from "typeorm";

@Index("categories_pkey", ["id"], { unique: true })
@Index("categories_name_key", ["name"], { unique: true })
@Entity("category", { schema: "public" })
export class Category extends BaseEntity {
  @Column("uuid", {
    primary: true,
    name: "id",
    default: () => "uuid_generate_v4()",
  })
  id: string;

  @Column("character varying", { name: "name", unique: true, length: 50 })
  name: string;

  @Column("text", { name: "description", nullable: true })
  description: string | null;

  @Column("timestamp without time zone", {
    name: "created_at",
    nullable: true,
    default: () => "CURRENT_TIMESTAMP",
  })
  createdAt: Date | null;

    @OneToMany(() => Book, (book) => book.category)
    books: Book[];

}
