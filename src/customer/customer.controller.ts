import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  ValidationPipe,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import { ApiTags } from '@nestjs/swagger';
import {
  Crud,
  CrudAuth,
  CrudController,
  Override,
  ParsedRequest,
  ParsedBody,
  CrudRequest,
  CrudRequestInterceptor,
} from '@nestjsx/crud';
import { Customer } from './entities/customer.entity';
import { CustomerService } from './customer.service';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { JwtService } from '@nestjs/jwt';
import { CustomerGuard } from '../guards/customer.guard';
import { Public } from 'src/guards/public.decorator';
import { getUser } from './get-user.decorator';
import { AdminGuard } from 'src/guards/admin.guard';

@ApiTags('customer')
@Controller('customer')
export class CustomerController {
  constructor(
    public service: CustomerService,
  ) {}
  @Post('/login')
  UserLogin(
    @Body(ValidationPipe) dto: CreateCustomerDto,
  ) {
    return this.service.login(dto);
  }

  @Post('/create')
  createCustomer(
    @Body() dto: Customer,
  ) {
    return this.service.createCustomer(dto);
  }

  @UseGuards(CustomerGuard)
  @Get('/me')
  getMe(
    @getUser() user:Customer
  ){
    return this.service.getMe(user)
  }

  @UseGuards(AdminGuard)
@Get('/:id')
getCustomer(
  @Param('id') id:string
){
  return this.service.getCustomer(id);
}

  @UseGuards(AdminGuard)
  @Put('delete/:id')
  deleteOne(
    @Param('id') id: string
  ){
    return this.service.delete(id);
  }

  @UseGuards(CustomerGuard)
  @Put('change-profile/:id')
  updateCustomer(
    @Body() dto:CreateCustomerDto,
    @Param('id') id:string
  ){
    return this.service.update(dto,id)
  }

  @UseGuards(AdminGuard) 
  @Get('/')
  getAll() {
    return this.service.getAll()
  }

}
