import { Module } from '@nestjs/common';
import { CustomerService } from './customer.service';
import { CustomerController } from './customer.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from './entities/customer.entity';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports:[
    TypeOrmModule.forFeature([Customer]),
    JwtModule.register({
      secret: 'emvuidi',
      signOptions: {
        expiresIn: '24h',
      },
    }),
  ],
  exports:[CustomerService],
  controllers: [CustomerController],
  providers: [CustomerService]
})
export class CustomerModule {}
