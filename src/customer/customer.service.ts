import { Injectable, Controller, BadRequestException, NotFoundException } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from './entities/customer.entity';
import { JwtService } from '@nestjs/jwt';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { DeleteResult } from 'typeorm';
@Injectable()
export class CustomerService extends TypeOrmCrudService<Customer> {
  constructor(@InjectRepository(Customer) repo, private jwt: JwtService) {
    super(repo);
  }
  async createCustomer(dto: Customer) {
    console.log(dto);
    const user = await this.repo.find({ email: dto.email });
    console.log(user);
    if (user.length > 0) {
      throw new BadRequestException('email exists');
    }
    const newUser = await this.repo.create(dto);
    await newUser.save();
    const payload = {
      email: newUser.email,
    };
    const token = this.jwt.sign(payload);
    return { token };
  }
  async login(dto: CreateCustomerDto) {
    const user = await this.repo.findOne(dto);
    if (user) {
      const payload = {
        email: user.email,
      };
      const token = this.jwt.sign(payload);
      return { token };
    }
    throw new BadRequestException('Authentication fail');
  }
  async getMe(user:Customer) {
    return await this.repo.findOne({
      where:{
        id:user.id
      },
      relations:['cart','reviews','orders','cart.cartBooks']
    })
  }

  async getCustomer(id:string){
    return await this.repo.findOne({
      where:{
        id:id
      },
      relations:['cart','reviews','orders','cart.cartBooks']
    })
  }

  async update(dto,id) {
    const customer = await this.repo.findOne({id});
    if(!customer) {
      throw new NotFoundException();
    }
    await this.repo.update({id},dto);
    return {message:"Update Sucessfully"}
  }

  async delete(id) {
    const customer = await this.repo.findOne({id});
  if(!customer) {
    throw new NotFoundException();
  }
  await this.repo.update({id},{deleted: true});
  return {message:"Deleted Sucessfully"}
}


async getAll() {
  return await this.repo.find({
    order:{
      createdAt:'DESC'
    },
  })
}
}
