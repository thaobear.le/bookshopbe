import { IsNotEmpty, IsEmail } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @IsEmail()
  email:string

  @IsNotEmpty()
  password:string
}
