import { Cart } from "src/cart/entities/cart.entity";
import { Order } from "src/order/entities/order.entity";
import { Review } from "src/review/entities/review.entity";
import { BaseEntity, Column, Entity, Index, OneToMany, OneToOne } from "typeorm";
import { IsNotEmpty } from "class-validator";

@Index("customer_pkey", ["id"], { unique: true })
@Entity("customer", { schema: "public" })
export class Customer extends BaseEntity {
  @Column("uuid", {
    primary: true,
    name: "id",
    default: () => "uuid_generate_v4()",
  })
  id: string;

  @IsNotEmpty()
  @Column("character varying", { name: "email", length: 255 })
  email: string;

  @Column("character varying", { name: "password", length: 255 })
  password: string;

  @Column("character varying", { name: "full_name", length: 255 })
  fullName: string;

  @Column("smallint", {
    name: "loyal_point",
    nullable: true,
    default: () => "0",
  })
  loyalPoint: number | null;

  @Column("date", { name: "birthday", nullable: true})
  birthday: Date | null;

  @Column("boolean",{name:"deleted", nullable:true, default: () => "false" })
  deleted: boolean | null;

  @Column("character varying", { name: "phone", nullable: true, length: 20 })
  phone: string | null;

  @Column("character varying", { name: "address", nullable: true, length: 255 })
  address: string | null;

  @Column("character varying", { name: "city", nullable: true, length: 255 })
  city: string | null;

  @Column("character varying", { name: "country", nullable: true, length: 255 })
  country: string | null;

  @Column("timestamp without time zone", {
    name: "created_at",
    nullable: true,
    default: () => "CURRENT_TIMESTAMP",
  })
  createdAt: Date | null;

  @Column("boolean", {
    name: "inactive",
    nullable: true,
    default: () => "false",
  })
  inactive: boolean | null;

  @OneToOne(() => Cart, (cart) => cart.customer)
  cart: Cart;

  @OneToMany(() => Order, (order) => order.customer)
  orders: Order[];

  @OneToMany(() => Review, (review) => review.customer)
  reviews: Review[];
}
