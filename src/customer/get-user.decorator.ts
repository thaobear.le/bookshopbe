import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Customer } from './entities/customer.entity';


export const getUser = createParamDecorator(
    (data: unknown, ctx: ExecutionContext): Customer => {
        const request = ctx.switchToHttp().getRequest();
        return request.user;
    },
);
