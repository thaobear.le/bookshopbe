import { Injectable, CanActivate, ExecutionContext, HttpException, HttpStatus, Inject } from "@nestjs/common";
import * as jwt from 'jsonwebtoken';
import { AdminService } from "src/admin-sang/admin.service";
import { getRepository } from "typeorm";
import { Customer } from '../customer/entities/customer.entity';
import { Reflector } from "@nestjs/core";
@Injectable()
export class CustomerGuard implements CanActivate {
    constructor( private readonly reflector: Reflector) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const isPublic = this.reflector.get<boolean>( "isPublic", context.getHandler() );
        if(isPublic) {
            return true;
        }
        const request = context.switchToHttp().getRequest();
        if (!request.headers.authorization) {
            return false
        }
        request.token = await this.validateToken(request.headers.authorization)


        request.user = await getRepository(Customer).findOne({email:request.token.email})
        console.log(request.user)
        return true
    }

    async validateToken(auth: string) {
        if (auth.split(' ')[0] !== 'Bearer') {
            throw new HttpException('Invalid token', HttpStatus.FORBIDDEN)
        }
        const token = auth.split(' ')[1]
        try {
            const decoded = jwt.verify(token, process.env.JWT_SECRET)
            return decoded
        } catch (error) {
            if (error.name == "TokenExpiredError") {
                throw new HttpException(error.name, HttpStatus.NOT_ACCEPTABLE)
            }
            const message = 'Token error: ' + (error.message || error.name)
            throw new HttpException(message, HttpStatus.FORBIDDEN)
        }
    }
}