import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import {SwaggerModule,DocumentBuilder} from '@nestjs/swagger'
import createSubscriber from "pg-listen";
import * as bodyParser from 'body-parser';

 
async function bootstrap() {
  const logger = new Logger('bootstrap')
  const app = await NestFactory.create(AppModule,{cors:true});
  // app.use(bodyParser.json({limit: '50mb'}));
  // app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
    // Payload as passed to subscriber.notify() (see below)
  const options = new DocumentBuilder()
    .setTitle('My API')
    .setDescription('API description')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);


  await app.listen(process.env.PORT || 8888);

  logger.log(`Environment: ${process.env.NODE_ENV}`);
}


bootstrap();
