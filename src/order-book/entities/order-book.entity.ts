import { Book } from "src/book/entities/book.entity";
import { Order } from "src/order/entities/order.entity";
import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
  } from "typeorm";
  @Index("order_book_pkey", ["bookId", "orderId"], { unique: true })
  @Entity("order_book", { schema: "public" })
  export class OrderBook extends BaseEntity {
    @Column("uuid", { primary: true, name: "order_id" })
    orderId: string;
  
    @Column("uuid", { primary: true, name: "book_id" })
    bookId: string;
  
    @Column("smallint", { name: "quantity", nullable: true })
    quantity: number | null;
  
    
    @Column("timestamp without time zone", {
      name: "created_at",
      nullable: true,
      default: () => "CURRENT_TIMESTAMP",
    })
    createdAt: Date | null;
    @Column("smallint", { name: "discount", nullable: true })
    discount: number | null
    
    @Column("integer", { name: "price_per_unit", nullable: true })
    pricePerUnit: number | null;

    @ManyToOne(() => Book, (book) => book.orderBooks)
    @JoinColumn([{ name: "book_id", referencedColumnName: "id" }])
    book: Book;
  
    @ManyToOne(() => Order, (order) => order.orderBooks)
    @JoinColumn([{ name: "order_id", referencedColumnName: "id" }])
    order: Order;
  }
  