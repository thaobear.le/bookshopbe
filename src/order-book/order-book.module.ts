import { Module } from '@nestjs/common';
import { OrderBookService } from './order-book.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderBook } from './entities/order-book.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([OrderBook])
  ],
  providers: [OrderBookService]
})
export class OrderBookModule {}
