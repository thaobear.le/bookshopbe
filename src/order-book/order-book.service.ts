import { Injectable, Controller } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import {TypeOrmCrudService} from '@nestjsx/crud-typeorm'
import { InjectRepository } from '@nestjs/typeorm';
import { OrderBook } from './entities/order-book.entity';
@Injectable()
export class OrderBookService extends TypeOrmCrudService<OrderBook> {
  constructor(@InjectRepository(OrderBook) repo) {
    super(repo);
  } 
}