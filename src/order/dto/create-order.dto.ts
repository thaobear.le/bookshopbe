import { IsNotEmpty, IsString, IsBoolean, IsEmail, MinLength, IsArray, ValidationTypes, ValidateNested } from 'class-validator';
import {Type} from 'class-transformer'



export class CreateOrderDto {

  tax:number;
  shippingPrice:number;
  address : string;
  promotionId: string;
  // promotionValue:number;

}
