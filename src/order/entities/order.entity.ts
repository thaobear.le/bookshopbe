import { Customer } from "src/customer/entities/customer.entity";
import { OrderBook } from "src/order-book/entities/order-book.entity";
import { Promotion } from "src/promotion/entities/promotion.entity";
import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
  } from "typeorm";
  export enum StatusOrder {
    NEW = "new",
     PACKED = "packed",
     IN_TRANSIT= "in transit",
     DELIVERED ="delivered"
 }
 
  @Index("order_pkey", ["id"], { unique: true })
  @Entity("order", { schema: "public" })
 

  export class Order extends BaseEntity {
    @Column("uuid", {
      primary: true,
      name: "id",
      default: () => "uuid_generate_v4()",
    })
    id: string;
 
    @Column()
    customerId:string
    
    @Column("smallint", { name: "total_quantity", nullable: true })
    totalQuantity: number | null;

    @Column()
    promotionId:string
  
    @Column("integer", { name: "total_book_price", nullable: true })
    totalBookPrice: number | null;
  
    // @Column("smallint",{name: "promotion_value"})
    // promotionValue: number| null;


    @Column("varchar",{name:"address", length:255,nullable:false})
    address: string;

    @Column("smallint", { name: "tax", nullable: true })
    tax: number | null;
  
    @Column("integer", { name: "shipping_price", nullable: true })
    shippingPrice: number | null;
  
    @Column("timestamp without time zone", {
      name: "created_at",
      nullable: true,
      default: () => "CURRENT_TIMESTAMP",
    })
    createdAt: Date | null;
  
    @ManyToOne(() => Customer, (customer) => customer.orders)
    @JoinColumn([{ name: "customer_id", referencedColumnName: "id" }])
    customer: Customer;
  
    @ManyToOne(() => Promotion, (promotion) => promotion.orders)
    @JoinColumn([{ name: "promotion_id", referencedColumnName: "id" }])
    promotion: Promotion;
  
    @OneToMany(() => OrderBook, (orderBook) => orderBook.order)
    orderBooks: OrderBook[];

   
    @Column({
      name:"status",
      type: "enum",
      enum: StatusOrder,
      default: StatusOrder.NEW
  })
  status: StatusOrder;
  }
  