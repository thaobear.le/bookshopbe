import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, ValidationPipe } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import { OrderService } from './order.service';
import { CustomerGuard } from '../guards/customer.guard';
import { ApiTags } from '@nestjs/swagger';
import { CreateOrderDto } from './dto/create-order.dto';
import { getUser } from '../customer/get-user.decorator';
import { Customer } from '../customer/entities/customer.entity';
import { UpdateBookDto } from 'src/book/dto/update-book.dto';
import { AdminGuard } from 'src/guards/admin.guard';
import { Order } from './entities/order.entity';

@ApiTags('Order')
@UseGuards(CustomerGuard)
@Controller('order')
export class OrderController {

  constructor(public service: OrderService) {}
  @Post()
  createOrder(
    @Body() dto:CreateOrderDto,
    @getUser() user:Customer,
  ) {
    return this.service.createOrder(dto,user)
  }

  @UseGuards(AdminGuard)
  @Get('/')
  getAll(){
    return this.service.getAll();
  }
  
  @Get('/my-list')
  getOrders(
    @getUser() user:Customer,
  ) {
    return this.service.getOrders(user)
  }

  @Get('/:id')
  getOrder(
    @getUser() user:Customer,
    @Param('id') id: string
  ) {
    return this.service.getOrder(id,user)
  }

  @Put('/:id')
  updateOrder(
    @Body() dto:Order,
    @Param('id') id:string
  ){
    return this.service.update(dto,id)
  }
}
