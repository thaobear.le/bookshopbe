import { Injectable, Controller, NotFoundException } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { CreateOrderDto } from './dto/create-order.dto';
import { Customer } from '../customer/entities/customer.entity';
import { getRepository } from 'typeorm';
import { Cart } from '../cart/entities/cart.entity';
import { CartBook } from '../cart-book/entities/cart-book.entity';
import { OrderBook } from '../order-book/entities/order-book.entity';
import { async } from 'rxjs';
import { Book } from '../book/entities/book.entity';
@Injectable()
export class OrderService extends TypeOrmCrudService<Order> {
  constructor(@InjectRepository(Order) repo) {
    super(repo);
  }

  async createOrder(dto: CreateOrderDto, user: Customer) {
    try {
      const cart = await getRepository(Cart).findOne({ customerId: user.id });
      const order = await this.repo.create({
        customerId: user.id,
        totalBookPrice: cart.totalBookPrice,
        totalQuantity: cart.totalQuantity,
        tax: dto.tax,
        shippingPrice: dto.shippingPrice,
        promotionId: dto.promotionId,
        // promotionValue: dto.promotionValue,

      });
      await order.save();
      const cartBook = await getRepository(CartBook).find({
        where: { cartId: cart.id },
        relations: ['book'],
        // select: ['bookId', 'quantity','pricePerUnit'],
      });

      const data = cartBook.map(item => {
        let a = {};
        a = item;
        a['orderId'] = order.id;
        a['pricePerUnit'] = item.book.pricePerUnit;
        a['discount'] = item.book.discount;
        return a;
      });
      await getRepository(OrderBook).insert(data);
      Promise.all(
        cartBook.map(async item => {
          const book = await getRepository(Book).findOne({ id: item.bookId });
          book.quantity = Number(book.quantity) - Number(item.quantity);
          await book.save();
        }),
      );
      cart.totalQuantity = 0;
      cart.totalBookPrice= 0;
      await cart.save();
      await getRepository(CartBook).delete({ cartId: cart.id });
      return await this.repo.findOne(
        {
          where: { id: order.id },
          relations: ['orderBooks','orderBooks.book'],
        });
  
    } catch (error) {
      return { message: error.message };
    }
  }

  async getOrders(user: Customer) {
    return await this.repo.find({
      where: { customerId: user.id },
      relations: ['orderBooks','orderBooks.book'],
    });
  }

  async getAll(){
    return await this.repo.find({relations: ['customer','orderBooks','orderBooks.book', 'promotion'],});

  }
  
  async getOrder(id, user: Customer) {
    return await this.repo.findOne({
      where: { customerId: user.id, id },
      relations: ['orderBooks','orderBooks.book'],
    });
  }

  async update(dto,id) {
    const order = await this.repo.findOne({id});
    if(!order) {
      throw new NotFoundException();
    }
    await this.repo.update({id},dto);
    return await this.repo.findOne({id}) 
  }
}
