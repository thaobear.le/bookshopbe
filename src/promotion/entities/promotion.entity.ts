import { Order } from "src/order/entities/order.entity";
import { BaseEntity, Column, Entity, Index, OneToMany } from "typeorm";

@Index("promotion_pkey", ["id"], { unique: true })
@Entity("promotion", { schema: "public" })
export class Promotion extends BaseEntity {
  @Column("uuid", {
    primary: true,
    name: "id",
    default: () => "uuid_generate_v4()",
  })
  id: string;

  @Column("smallint", { name: "loyal_point", nullable: true })
  loyalPoint: number | null;

  @Column("smallint", { name: "discount" })
  discount: number;

  @Column("varchar",{name:"title", length:255})
  title: string;

  @Column("integer", { name: "max_discount_price", nullable: true })
  maxDiscountPrice: number | null;

  @Column("integer", { name: "min_accept_order_price", nullable: true })
  minAcceptOrderPrice: number | null;

  @Column("timestamp without time zone", { name: "end_date", nullable: true })
  endDate: Date | null;

  @Column("timestamp without time zone", { name: "start_date", nullable: true })
  startDate: Date | null;

  @Column("smallint", { name: "quantity", nullable: true })
  quantity: number | null;

  @Column("boolean", {
    name: "is_deleted",
    nullable: true,
    default: () => "false",
  })
  isDeleted: boolean | null;

  @Column("timestamp without time zone", {
    name: "created_at",
    nullable: true,
    default: () => "CURRENT_TIMESTAMP",
  })
  createdAt: Date | null;

  @OneToMany(() => Order, (order) => order.promotion)
  orders: Order[];
  
}
