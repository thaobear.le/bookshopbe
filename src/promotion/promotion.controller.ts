
import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from "@nestjsx/crud";
import { Promotion } from './entities/promotion.entity';
import { PromotionService } from './promotion.service';

@ApiTags('Promotion')
@Crud({
  model:{
    type:Promotion,
  },
  params: {
    id: {
      field:'id',
      type:'uuid',
      primary:true
    }
  },
})
@Controller('promotion')
export class PromotionController implements CrudController<Promotion> {
  constructor(public service: PromotionService) {}

}
