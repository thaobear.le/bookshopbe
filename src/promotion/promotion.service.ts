import { Injectable, Controller } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import {TypeOrmCrudService} from '@nestjsx/crud-typeorm'
import { InjectRepository } from '@nestjs/typeorm';
import { Promotion } from './entities/promotion.entity';
@Injectable()
export class PromotionService extends TypeOrmCrudService<Promotion> {
  constructor(@InjectRepository(Promotion) repo) {
    super(repo);
  } 
}