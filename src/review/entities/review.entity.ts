import { Book } from "src/book/entities/book.entity";
import { Customer } from "src/customer/entities/customer.entity";
import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
  } from "typeorm";
  
  @Index("review_pkey", ["id"], { unique: true })
  @Entity("review", { schema: "public" })
  export class Review extends BaseEntity {
    @Column("uuid", {
      primary: true,
      name: "id",
      default: () => "uuid_generate_v4()",
    })
    id: string;

    @Column()
    bookId:string;
  
    @Column()
    customerId:string;

    @Column("smallint", { name: "rate", nullable: true })
    rate: number | null;
  
    @Column("text", { name: "content" })
    content: string;
  
    @Column("timestamp without time zone", {
      name: "created_at",
      nullable: true,
      default: () => "CURRENT_TIMESTAMP",
    })
    createdAt: Date | null;
  
    @ManyToOne(() => Book, (book) => book.reviews)
    @JoinColumn([{ name: "book_id", referencedColumnName: "id" }])
    book: Book;
  
    @ManyToOne(() => Customer, (customer) => customer.reviews)
    @JoinColumn([{ name: "customer_id", referencedColumnName: "id" }])
    customer: Customer;
  }
  