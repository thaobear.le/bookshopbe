import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, ValidationPipe } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController, CrudAuth } from "@nestjsx/crud";
import { Review } from './entities/review.entity';
import { ReviewService } from './review.service';
import { Customer } from '../customer/entities/customer.entity';
import { CustomerGuard } from '../guards/customer.guard';
import { getUser } from '../customer/get-user.decorator';
import { CreateReviewDto } from './dto/create-review.dto';
@ApiTags('Review')
@UseGuards(CustomerGuard)
@Controller('review')
export class ReviewController {
  constructor(public service: ReviewService) {}
  
  @Post()
  sendReivew(
    @Body(ValidationPipe) dto :CreateReviewDto,
    @getUser() user: Customer
  ) {
    return this.service.sendReview(dto,user);
  }
}
