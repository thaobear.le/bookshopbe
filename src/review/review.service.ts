import { Injectable, Controller } from '@nestjs/common';
// import { CreateBookDto } from './dto/create-book.dto';
// import { UpdateBookDto } from './dto/update-book.dto';
import {TypeOrmCrudService} from '@nestjsx/crud-typeorm'
import { InjectRepository } from '@nestjs/typeorm';
import { Review } from './entities/review.entity';
import { CreateReviewDto } from './dto/create-review.dto';
import { Customer } from '../customer/entities/customer.entity';
import { getRepository } from 'typeorm';
import { Book } from '../book/entities/book.entity';
import { BookImage } from '../book-image/entities/book-image.entity';
@Injectable()
export class ReviewService extends TypeOrmCrudService<Review> {
  constructor(@InjectRepository(Review) repo) {
    super(repo);
  } 
  async sendReview(dto:CreateReviewDto,user:Customer) {
    const review = await this.repo.create({...dto,customerId:user.id});
    const query = await this.repo.createQueryBuilder('r')
      .select('AVG(r.rate) as avg')
      .where('r.bookId = :id',{id:dto.bookId}).getRawOne();
    const book = await getRepository(Book).findOne({id:dto.bookId});
    await getRepository(Book).update({id:dto.bookId},{rate:query.avg,countRate:Number(book.countRate) + 1})
    await review.save();
    return review
  }
}